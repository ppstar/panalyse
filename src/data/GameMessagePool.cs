﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using lineage.com.interfaces;
using lineage.com.model;
using lineage.com.game.login.message;
using lineage.com.game.login.handler;
using lineage.com.game.backpack.message;
using lineage.com.game.backpack.handler;
using lineage.com.game.quest.message;
using lineage.com.game.quest.handler;
using lineage.com.game.forge.message;
using lineage.com.game.forge.handler;
using lineage.com.game.move.message;
using lineage.com.game.actionobject.message;
using lineage.com.game.actionobject.handler;
using lineage.com.game.move.handler;
using lineage.com.game.skill.message;
using lineage.com.game.skill.handler;
using lineage.com.game.shop.message;
using lineage.com.game.shop.handler;
using lineage.com.game.gameevent.message;
using lineage.com.game.gameevent.handler;
using lineage.com.game.achivement.message;
using lineage.com.game.achivement.handler;
using lineage.com.game.dungeon.message;
using lineage.com.game.dungeon.handler;
using lineage.com.game.rune.message;
using lineage.com.game.rune.handler;
using lineage.com.game.world.message;
using lineage.com.game.world.handler;
using lineage.com.game.content.message;
using lineage.com.game.content.handler;
using lineage.com.game.monsterbook.message;
using lineage.com.game.monsterbook.handler;
using lineage.com.game.friend.message;
using lineage.com.game.friend.handler;
using lineage.com.game.friend.request;
using lineage.com.game.mission.message;
using lineage.com.game.mission.handler;
using lineage.com.game.pvp.message;
using lineage.com.game.pvp.handler;
using lineage.com.game.party.message;
using lineage.com.game.party.handler;
using lineage.com.game.auction.message;
using lineage.com.game.auction.handler;
using lineage.com.game.guild.handler;
using lineage.com.game.guild.message;
using lineage.com.game.diamond.message;
using lineage.com.game.diamond.handler;
using lineage.com.game.mail.message;
using lineage.com.game.mail.handler;
using lineage.com.game.elixir.message;
using lineage.com.game.elixir.handler;
using lineage.com.game.time.handler;
using lineage.com.game.time.message;
using lineage.com.game.classtransfer.message;
using lineage.com.game.classtransfer.handler;

namespace lineage.com.game
{
    class GameMessagePool : MessagePool
    {
        public GameMessagePool(IObject obj)
            : base(obj)
        {

        }

        protected override void registHandlers()
        {
            this.register(0x04, typeof(PktLoginResult), typeof(PktLoginHandler));
            this.register(0x0D, typeof(PktTimeSyncResult), typeof(PktTimeSyncResultHandler));        //서버와 시간 동기화 응답
            this.register(0x0F, typeof(PktKickoutNotify), typeof(PktKickoutNotifyHandler)); 
            this.register(0x11, typeof(PktWaitingNumUpdateNotify), typeof(PktWaitingNumUpdateNotifyHandler));   //서버대기
            this.register(0x14, typeof(PktPlayerListReadResult), typeof(PktPlayerListReadHandler));
            this.register(0x16, typeof(PktPlayerCreateResult), typeof(PktPlayerCreateResultHandler));
            this.register(0x1A, typeof(PktPlayerSelectResult), typeof(PktPlayerSelectResultHandler));
            this.register(0x1C, typeof(PktSystemTutorialStartResult), typeof(PktSystemTutorialStartResultHandler));
            this.register(0x2A, typeof(PktVersionResult), typeof(PktVersionHandler));
            this.register(0x2C, typeof(PktAccountLinkInfoResult), typeof(PktAccountLinkInfoResultHandler));
            
            this.register(0x66, typeof(PktWorldMoveReserveResult), typeof(PktWorldMoveReserveHandler));
            this.register(0x68, typeof(PktWorldMoveStartResult), typeof(PktWorldMoveStartHandler));
            this.register(0x6A, typeof(PktWorldMoveFinishResult), typeof(PktWorldMoveFinishHandler));
            this.register(0x6C, typeof(PktWorldChannelListResult), typeof(PktWorldChannelListResultHandler));
            this.register(0x6E, typeof(PktWorldChannelMoveReserveResult), typeof(PktWorldChannelMoveReserveResultHandler));            
            this.register(0xCB, typeof(PktPlayerMoveNotify), typeof(PktPlayerMoveNotifyHandler));  //한개 플레이어 움직임
            this.register(0xCC, typeof(PktPlayerMoveListNotify), typeof(PktPlayerMoveListNotifyHandler));  //여러 플레이어 움직임
            this.register(0xCD, typeof(PktSightEnterNotify), typeof(PktSightEnterNotifyHandler));   //AO 추가 (player, npc .. )
            this.register(0xCE, typeof(PktSightLeaveNotify), typeof(PktSightLeaveNotifyHandler));   //AO 삭제
            this.register(0xCF, typeof(PktNpcMoveNotify), typeof(PktNpcMoveNotifyHandler));         //NPC 움직임
            this.register(0xD1, typeof(PktActorDestroyNotify), typeof(PktActorDestroyNotifyHandler));//각종 액션오브젝트 죽음..(나 포함)
            this.register(0xD3, typeof(PktPlayerReviveResult), typeof(PktPlayerReviveResultHandler));//캐랙터 부활처리
            this.register(0xD7, typeof(PktNpcCreateNotify), typeof(PktNpcCreateNotifyHandler));     //NPC 추가
            this.register(0xDC, typeof(PktCharacterLevelUpNotify), typeof(PktCharacterLevelUpNotifyHandler));     //자기 캐릭터, 유저 레벨업
            this.register(0xDF, typeof(PktCharacterStatChangeNotify), typeof(PktCharacterStatChangeNotifyHandler));      //player 상태변경
            this.register(0xE2, typeof(PktActorStateChangeNotify), typeof(PktActorStateChangeNotifyHandler));           //npc 상태변경
            this.register(0xED, typeof(PktGadgetCreateNotify), typeof(PktGadgetCreateNotifyHandler));           //Gadget 출현
            this.register(0xEF, typeof(PktGadgetControlStartResult), typeof(PktGadgetControlStartResultHandler));
            this.register(0x107, typeof(PktContentUnlockListReadResult), typeof(PktContentUnlockListReadResultHandler));//콘텐츠 언록 정보            
            this.register(0x109, typeof(PktContentUnlockRequestResult), typeof(PktContentUnlockRequestResultHandler));//콘텐츠 언록 정보            
            this.register(0x10F, typeof(PktBattleEndNotify), typeof(PktBattleEndNotifyHandler));//결투장 종료            
            this.register(0x111, typeof(PktTargetActorChangeResult), typeof(PktTargetActorChangeResultHandler)); //타겟 변경
            this.register(0x114, typeof(PktLevelUpGiftResult), typeof(PktLevelUpGiftResultHandler)); //레벨업 보상
            this.register(0x12E, typeof(PktAchievementListReadResult), typeof(PktAchievementListReadResultHandler));
            this.register(0x133, typeof(PktAchievementRewardGetResult), typeof(PktAchievementRewardGetResultHandler));
            this.register(0x137, typeof(PktAchievementLevelRewardGetResult), typeof(PktAchievementLevelRewardGetResultHandler));
            this.register(0x160, typeof(PktDailyActivityListReadResult), typeof(PktDailyActivityListReadResultHandler));  //하루일과정보
            this.register(0x163, typeof(PktDailyActivityRewardGetResult), typeof(PktDailyActivityRewardGetResultHandler));  //하루일과보상 결과
            this.register(0x165, typeof(PktDailyActivityPointRewardGetResult), typeof(PktDailyActivityPointRewardGetResultHandler));  //하루일과포인트보상 결과
            this.register(0x192, typeof(PktRuneInfoReadResult), typeof(PktRuneInfoReadResultHandler));          //룬 정보 
            this.register(0x194, typeof(PktRuneLevelUpResult), typeof(PktRuneLevelUpResultHandler)); 
			this.register(0x1C4, typeof(PktMonsterBookListReadResult), typeof(PktMonsterBookListReadResultHandler));
            this.register(0x1C6, typeof(PktMonsterCoreUseResult), typeof(PktMonsterCoreUseResultHandler));
            this.register(0x1F7, typeof(PktRestRewardChangeNotify), typeof(PktRestRewardChangeNotifyHandler));     //휴식보상정보   
            this.register(0x228, typeof(PktAttendanceReadResult), typeof(PktAttendanceReadHandler));     //접속보상정보
            this.register(0x22E, typeof(PktAttendanceWeeklyGetResult), typeof(PktAttendanceWeeklyGetResultHandler));     //주간보상정보
            this.register(0x25D, typeof(PktSkillStartResult), typeof(PktSkillStartResultHandler));     //스킬사용
            this.register(0x25E, typeof(PktSkillStartNotify), typeof(PktSkillStartNotifyHandler));     //스킬사용
            

            this.register(0x264, typeof(PktSkillHitNotify), typeof(PktSkillHitNotifyHandler));          //스킬사용결과 (hp 변경)
            this.register(0x266, typeof(PktSkillCoolTimeResetNotify), typeof(PktSkillCoolTimeResetNotifyHandler));          //스킬클타임 리셋
            this.register(0x26E, typeof(PktBuffAddNotify), typeof(PktBuffAddNotifyHandler));            //buff추가
            this.register(0x26F, typeof(PktBuffRemoveNotify), typeof(PktBuffRemoveNotifyHandler));      //buff삭제
            this.register(0x273, typeof(PktSkillListReadResult), typeof(PktSkillListReadHandler));
            this.register(0x275, typeof(PktSkillUpgradeResult), typeof(PktSkillUpgradeResultHandler));
            this.register(0x277, typeof(PktSkillDeckEquipResult), typeof(PktSkillDeckEquipResultHandler));
            this.register(0x27A, typeof(PktSkillBookUseResult), typeof(PktSkillBookUseResultHandler));        
            this.register(0x27E, typeof(PktSoulShotEnableChangeNotify), typeof(PktSoulShotEnableChangeNotifyHandler));
            this.register(0x27F, typeof(PktSoulShotItemDeleteNotify), typeof(PktSoulShotItemDeleteNotifyHandler));
            this.register(0x2F3, typeof(PktChatChannelListReadResult), typeof(PktChatChannelListReadHandler));
            this.register(0x31A, typeof(PktChatPromoListReadResult), typeof(PktChatPromoListReadResultHandler));
            this.register(0x354, typeof(PktBagListReadResult), typeof(PktBagListReadHandler));
            this.register(0x356, typeof(PktEquipmentListReadResult), typeof(PktEquipmentListReadHandler));
            this.register(0x359, typeof(PktItemEquipResult), typeof(PktItemEquipResultHandler));
            this.register(0x35C, typeof(PktItemUnequipResult), typeof(PktItemUnequipResultHandler));
            this.register(0x363, typeof(PktItemSellResult), typeof(PktItemSellResultHandler));
            this.register(0x367, typeof(PktItemLevelUpResult), typeof(PktItemLevelUpResultHandler));
            this.register(0x36D, typeof(PktItemEnchantResult), typeof(PktItemEnchantResultHandler));
            this.register(0x370, typeof(PktItemUpgradeResult), typeof(PktItemUpgradeResultHandler));
            this.register(0x374, typeof(PktItemComposeResult), typeof(PktItemComposeResultHandler));
            this.register(0x37B, typeof(PktItemUseResult), typeof(PktItemUseResultHandler));
            this.register(0x37E, typeof(PktSoulCrystalEquipResult), typeof(PktSoulCrystalEquipResultHandler));
            

            this.register(0x386, typeof(PktItemCraftResult), typeof(PktItemCraftResultHandler));
            this.register(0x387, typeof(PktItemLootNotify), typeof(PktItemLootNotifyHandler));
            this.register(0x389, typeof(PktBagExpandResult), typeof(PktBagExpandResultHandler));
			this.register(0x399, typeof(PktElixirInfoReadResult), typeof(PktElixirInfoReadResultHandler));
            this.register(0x3EA, typeof(PktMailListReadResult), typeof(PktMailListReadResultHandler));
            this.register(0x3F0, typeof(PktMailRewardGetResult), typeof(PktMailRewardGetResultHandler));     //우편받기결과
            this.register(0x3F2, typeof(PktMailRewardAllGetResult), typeof(PktMailRewardAllGetResultHandler));     //전체우편받기결과   
            this.register(0x3F3, typeof(PktMailReceiveNotify), typeof(PktMailReceiveNotifyHandler));     //우편수신   
            this.register(0x41C, typeof(PktQuestStartResult), typeof(PktQuestStartResultHandler));
            this.register(0x41E, typeof(PktQuestCancelResult), typeof(PktQuestCancelResultHandler));
            this.register(0x422, typeof(PktQuestUpdateResult), typeof(PktQuestUpdateResultHandler));
            this.register(0x423, typeof(PktQuestUpdateNotify), typeof(PktQuestUpdateNotifyHandler));
            this.register(0x425, typeof(PktQuestGadgetControlResult), typeof(PktQuestGadgetControlResultHandler));
            this.register(0x429, typeof(PktQuestCompleteResult), typeof(PktQuestCompleteResultHandler));
            this.register(0x438, typeof(PktQuestActListReadResult), typeof(PktQuestActListReadResultHandler));
            this.register(0x43A, typeof(PktQuestActCompleteRewardGetResult), typeof(PktQuestActCompleteRewardGetResultHandler));
            this.register(0x43B, typeof(PktQuestActChapterCompleteNotify), typeof(PktQuestActChapterCompleteNotifyHandler));

            this.register(0x480, typeof(PktPartyInfoReadResult), typeof(PktPartyInfoReadResultHandler));
            this.register(0x48B, typeof(PktPartyInviteResult), typeof(PktPartyInviteResultHandler));
            this.register(0x48C, typeof(PktPartyInviteNotify), typeof(PktPartyInviteNotifyHandler));
            this.register(0x48E, typeof(PktPartyInviteAcceptResult), typeof(PktPartyInviteAcceptResultHandler));
            this.register(0x49F, typeof(PktPartyMemberJoinNotify), typeof(PktPartyMemberJoinNotifyHandler));
            
            this.register(0x4A4, typeof(PktPartyWithdrawResult), typeof(PktPartyWithdrawResultHandler));
            this.register(0x4A5, typeof(PktPartyWithdrawNotify), typeof(PktPartyWithdrawNotifyHandler));
            this.register(0x49E, typeof(PktPartyCreateNotify), typeof(PktPartyCreateNotifyHandler));
            this.register(0x4A1, typeof(PktPartyExpelResult), typeof(PktPartyExpelResultHandler));            
            this.register(0x4A2, typeof(PktPartyExpelNotify), typeof(PktPartyExpelNotifyHandler));                        
            this.register(0x4A7, typeof(PktPartyDisbandResult), typeof(PktPartyDisbandResultHandler));
            this.register(0x4A8, typeof(PktPartyDisbandNotify), typeof(PktPartyDisbandNotifyHandler));            
            this.register(0x4AA, typeof(PktPartyMasterEntrustResult), typeof(PktPartyMasterEntrustResultHandler));
            this.register(0x4AB, typeof(PktPartyMasterEntrustNotify), typeof(PktPartyMasterEntrustNotifyHandler));
            this.register(0x4CB, typeof(PktPartyMemberPosNotify), typeof(PktPartyMemberPosNotifyHandler));
            this.register(0x4CF, typeof(PktPartyAutoEnterResult), typeof(PktPartyAutoEnterResultHandler));
            this.register(0x4D4, typeof(PktPartyAutoEnterStartNotify), typeof(PktPartyAutoEnterStartNotifyHandler));
            this.register(0x4D6, typeof(PktPartyAutoEnterAcceptResult), typeof(PktPartyAutoEnterAcceptResultHandler));
            this.register(0x4D7, typeof(PktPartyAutoEnterAcceptNotify), typeof(PktPartyAutoEnterAcceptNotifyHandler));
            this.register(0x4E3, typeof(PktPartyVoiceChatJoinNotify), typeof(PktPartyVoiceChatJoinNotifyHandler));
            this.register(0x4E8, typeof(PktPartyMemberWorldMoveNotify), typeof(PktPartyMemberWorldMoveNotifyHandler));
            

            this.register(0x57A, typeof(PktGuildInfoReadResult), typeof(PktGuildInfoReadResultHandler));            //길드정보
            this.register(0x57C, typeof(PktGuildMemberListReadResult), typeof(PktGuildMemberListReadResultHandler));            //길드멤버
            this.register(0x57E, typeof(PktGuildCreateResult), typeof(PktGuildCreateResultHandler));            //길드생성 결과
            this.register(0x580, typeof(PktGuildCreateCheckResult), typeof(PktGuildCreateCheckResultHandler));  //길드이름 증복체크 결과            
            
            
            this.register(0x516, typeof(PktFriendListReadResult), typeof(PktFriendListReadResultHandler));                  //친구리스트
            this.register(0x51A, typeof(PktFriendInviterListReadResult), typeof(PktFriendInviterListReadResultHandler));    //친구요청 리스트
            this.register(0x51D, typeof(PktFriendInviteNotify), typeof(PktFriendInviteNotifyHandler));    //친구 요청받을떄
            this.register(0x529, typeof(PktFriendAcceptAllResult), typeof(PktFriendAcceptAllResultHandler));                      //친구요청 승락 응답
            this.register(0x535, typeof(PktFriendRecommendationListRefreshResult), typeof(PktFriendRecommendationListRefreshResultHandler));    //추천친구리스트
            this.register(0x537, typeof(PktFriendRecommendationInviteAllResult), typeof(PktFriendRecommendationInviteAllResultHandler));    //추천친구 요청응답
            this.register(0x53D, typeof(PktFriendGreetAllResult), typeof(PktFriendGreetAllResultHandler));                  //모두 인사하기
            this.register(0x542, typeof(PktFriendGreetReceiveAllResult), typeof(PktFriendGreetReceiveAllResultHandler));    //모두 인사받기

            this.register(0x5A1, typeof(PktGuildWithdrawResult), typeof(PktGuildWithdrawResultHandler));    //길드 탈퇴
			this.register(0x5BD, typeof(PktGuildDonateResult), typeof(PktGuildDonateResultHandler));    //길드 기부 결과
            this.register(0x5C3, typeof(PktGuildAttendResult), typeof(PktGuildAttendResultHandler));
            this.register(0x5C5, typeof(PktGuildAttendRewardGetResult), typeof(PktGuildAttendRewardGetResultHandler));
            this.register(0x5D0, typeof(PktGuildRecordListReadResult), typeof(PktGuildRecordListReadResultHandler));

            this.register(0x5DE, typeof(PktGuildMarketListReadResult), typeof(PktGuildMarketListReadResultHandler));
            this.register(0x5E2, typeof(PktGuildMarketGiftBuyResult), typeof(PktGuildMarketGiftBuyResultHandler));   
            this.register(0x5E6, typeof(PktGuildSearchResult), typeof(PktGuildSearchResultHandler));    //길드 목록 검색
            

            
            this.register(0x58A, typeof(PktGuildJoinRequestResult), typeof(PktGuildJoinRequestResultHandler));    //길드 가입 응답
            this.register(0x593, typeof(PktGuildJoinAcceptResult), typeof(PktGuildJoinAcceptResultHandler));
            this.register(0x596, typeof(PktGuildJoinAcceptNotify), typeof(PktGuildJoinAcceptNotifyHandler));    
            

            this.register(0x70A, typeof(PktAuctionHouseSearchListReadResult), typeof(PktAuctionHouseSearchListReadResultHandler));  //경매장아이템검색결과
            this.register(0x70C, typeof(PktAuctionHouseItemDetailViewResult), typeof(PktAuctionHouseItemDetailViewResultHandler));  //경매아이템 상세정보
            this.register(0x70E, typeof(PktAuctionHouseBuyResult), typeof(PktAuctionHouseBuyResultHandler));  //경매아이템 사기
            this.register(0x710, typeof(PktAuctionHouseSellingListReadResult), typeof(PktAuctionHouseSellingListReadResultHandler));  //판매중인 아이템 검색 결과
            this.register(0x712, typeof(PktAuctionHouseSellingRegisterResult), typeof(PktAuctionHouseSellingRegisterResultHandler));  //경매등록 결과
            this.register(0x714, typeof(PktAuctionHouseSellingCancelResult), typeof(PktAuctionHouseSellingCancelResultHandler));  //경매아이템 등록취소 결과            
            this.register(0x716, typeof(PktAuctionHouseSellingResultGetResult), typeof(PktAuctionHouseSellingResultGetResultHandler));  //팔린아이템 획득
            this.register(0x71C, typeof(PktAuctionHouseAveragePriceGetResult), typeof(PktAuctionHouseAveragePriceGetResultHandler));  //아이템 가격 요청

            this.register(0x76E, typeof(PktShopItemListReadResult), typeof(PktShopItemListReadResultHandler));
            this.register(0x770, typeof(PktShopItemBuyResult), typeof(PktShopItemBuyResultHandler));        //상점아이템 구입 결과
            this.register(0x774, typeof(PktFixedChargeDetailResult), typeof(PktFixedChargeDetailResultHandler));        //비귀속다이아 정보
            this.register(0x77A, typeof(PktFixedChargeDiamondGetResult), typeof(PktFixedChargeDiamondGetResultHandler));        //비귀속다이아받기
            this.register(0x785, typeof(PktSmartPopupNotify), typeof(PktSmartPopupNotifyHandler));        //광고판팟업
            
            this.register(0x7D2, typeof(PktDungeonListReadResult), typeof(PktDungeonListReadResultHandler));        //던전리스트 요청 결과
            this.register(0x7D4, typeof(PktDungeonEnterResult), typeof(PktDungeonEnterResultHandler));        //던전입장요쳥 응답
            this.register(0x7DA, typeof(PktDungeonSweepResult), typeof(PktDungeonSweepResultHandler));        //던전입장요쳥 응답
            this.register(0x7DB, typeof(PktDungeonResultNotify), typeof(PktDungeonResultNotifyHandler));        //던전클리어결과            
            this.register(0x7DC, typeof(PktDungeonStateNotify), typeof(PktDungeonStateNotifyHandler));      //던전 상태
            this.register(0x7DD, typeof(PktDungeonEndTimeNotify), typeof(PktDungeonEndTimeNotifyHandler));      //던전 종료시간            
            this.register(0x7DE, typeof(PktDungeonStartTimeNotify), typeof(PktDungeonStartTimeNotifyHandler));   //던전 시작까지 남은시간
            this.register(0x7E1, typeof(PktGatheringNotify), typeof(PktGatheringNotifyHandler));   //채집물 채집 완료시            
            this.register(0x7EC, typeof(PktDungeonHotTimeNotify), typeof(PktDungeonHotTimeNotifyHandler));   //핫타임 변화           
            this.register(0x836, typeof(PktPvpInfoReadResult), typeof(PktPvpInfoReadResultHandler));   //결투장 정보
            this.register(0x83A, typeof(PktPvpStartCheckResult), typeof(PktPvpStartCheckResultHandler));   //결투장 들어가기
            this.register(0x83C, typeof(PktPvpStartResult), typeof(PktPvpStartResultHandler));   //결투장 들어가기
            this.register(0x845, typeof(PktPvpRewardGetResult), typeof(PktPvpRewardGetResultHandler));   //결투장 보상
            this.register(0x848, typeof(PktPvpEndNotify), typeof(PktPvpEndNotifyHandler));   //결투장 완료

            this.register(0x89A, typeof(PktClassTransferQuestStartResult), typeof(PktClassTransferQuestStartResultHandler));
            this.register(0x89C, typeof(PktClassTransferQuestListReadResult), typeof(PktClassTransferQuestListReadResultHandler));
            this.register(0x89E, typeof(PktClassTransferQuestRewardGetResult), typeof(PktClassTransferQuestRewardGetResultHandler));
            this.register(0x8A0, typeof(PktClassTransferResult), typeof(PktClassTransferResultHandler));
            this.register(0x8A2, typeof(PktClassTransferQuestUpdateNotify), typeof(PktClassTransferQuestUpdateNotifyHandler));
            this.register(0x8A3, typeof(PktClassTransferQuestCompleteNotify), typeof(PktClassTransferQuestCompleteNotifyHandler));
            
            
            
            
            this.register(0x8FE, typeof(PktGuideQuestListReadResult), typeof(PktGuideQuestListReadResultHandler));   //용병단의 여정 정보
            this.register(0x900, typeof(PktGuideQuestRewardGetResult), typeof(PktGuideQuestRewardGetResultHandler));   //용병단의 여정 보상 획득
            this.register(0x902, typeof(PktGuideQuestCompleteNotify), typeof(PktGuideQuestCompleteNotifyHandler));   //용병단의 여정 보상

            this.register(0x965, typeof(PktTutorialStartResult), typeof(PktTutorialStartResultHandler));
            this.register(0x967, typeof(PktTutorialProgressResult), typeof(PktTutorialProgressResultHandler)); 
            this.register(0x96C, typeof(PktTutorialSkipResult), typeof(PktTutorialSkipResultHandler)); 
            this.register(0x9C5, typeof(PktCutSceneNotify), typeof(PktCutSceneNotifyHandler));   //보스 동영상 재생시작됨
            this.register(0x9CB, typeof(PktCutSceneEndReserveResult), typeof(PktCutSceneEndReserveResultHandler));
            this.register(0xA8E, typeof(PktMissionRequestListReadResult), typeof(PktMissionRequestListReadResultHandler)); //일일퀘스트목록
            this.register(0xA90, typeof(PktMissionRequestStartResult), typeof(PktMissionRequestStartResultHandler)); //일일수행
            this.register(0xA9A, typeof(PktMissionDailyRewardGetResult), typeof(PktMissionDailyRewardGetResultHandler)); //일일보상
            this.register(0xA9C, typeof(PktMissionWeeklyRewardGetResult), typeof(PktMissionWeeklyRewardGetResultHandler));
            this.register(0xA9E, typeof(PktMissionRequestRewardGetResult), typeof(PktMissionRequestRewardGetResultHandler));            
            this.register(0xA9F, typeof(PktMissionRequestCompleteNotify), typeof(PktMissionRequestCompleteNotifyHandler));
            this.register(0xAA1, typeof(PktMissionWeeklyUpdateNotify), typeof(PktMissionWeeklyUpdateNotifyHandler));            

            this.register(0xAC0, typeof(PktEventListReadResult), typeof(PktEventListReadResultHandler));            
        }
    }
}
