﻿#-*- coding: utf-8 -*-
import os
import zlib

class Decompress(object):
    
    #ZLIB 압축파일 해제
    @staticmethod
    def decompress(directory):
        if not os.path.isdir(directory): return
        files = [(directory + f) for f in os.listdir(directory) if os.path.isfile(directory + f)]

        for filepath in files:
            if not filepath.endswith('.log.z'): continue
            print(os.path.basename(filepath))
            with open(filepath, 'rb') as fp:
                data = fp.read()
                decompresed = zlib.decompress(data)
                Decompress.write(filepath, decompresed)


    @staticmethod
    def write(filepath, data):
        filepath += ".txt"
        with open(filepath, 'wb') as fp:
            fp.write(data)



if __name__ == '__main__':
    Decompress.decompress('D:\\ftp_root\\')