﻿#-*- coding: utf-8 -*-
from linrev.LinrevPacketHeader import *


class Linrev(object):
    def run(self):
        pass

    def make_packet_header_list(self):
        packets = LinrevPacketHeader.loadFile(os.getcwd() + '\\data\\packet_header.txt')
        if packets:
            LinrevPacketHeader.saveFile(packets,  os.getcwd() + '\\data\\packet_header.json')



if __name__ == '__main__':
    linrev = Linrev()
    linrev.make_packet_header_list()