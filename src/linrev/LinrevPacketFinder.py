﻿#-*- coding: utf-8 -*-
import os
import io
import json
from Utils import Utils


class LinrevPacketFinder(object):

    #소스폴더를 분석해서 요청파켓(SEND)정보를 얻는다.
    def generateKnownSendPacketFile(self, srcdir, outpath):
        files = self.getRequestPacketFileList(srcdir)
        if files == None: return False

        packets = []
        for filePath in files:
            #print(filePath)
            if filePath.find('com\\lobby\\') > 0: continue
            pkt = self.parseRequestPacketFile(filePath)
            if not pkt: raise Exception('Invalid Format File: ' + filePath)
            packets.append(pkt)

        self.writeKnownPacketFile(outpath, packets)


    #소스파일을 분석해서 RECV파켓정보를 얻는다.  
    def generateKnownRecvPacketFile(self, srcpath, outpath):
        packets = []
        with open(srcpath) as fp:
            for line in fp:
                if line.find("this.register") < 0: continue
                print(line)
                
                pkt = {'mode' : 'recv'}

                data = line.replace(' ', '')
                data = data.replace("this.register", "").replace("typeof", "").replace('(', '').replace(')', '').strip();

                pieces = data.split(',')
                if len(pieces) < 2: raise Exception('Invalid Format')

                pkt['id'] = int(pieces[0], 16)
                pkt['name'] = pieces[1]
                
                packets.append(pkt)

        self.writeKnownPacketFile(outpath, packets)


    
    def getRequestPacketFileList(self, directory):
        filepaths = Utils.getAllFileList(directory, '.cs')
        if not filepaths: return None

        files = []
        for file in filepaths:
            dirname = os.path.dirname(file)
            if dirname.lower().endswith('\\request'):
                files.append(file)

        return files


    def substring(self, str, startwith, endswith):
        s = str.find(startwith)
        if s < 0: return None
        s += len(startwith)
        e = str.find(endswith, s)
        if e < 0: return None
        if s == e: return None
        return str[s:e]



    def parseRequestPacketFile(self, filePath):
        if not os.path.isfile(filePath):
            print('File not exist')
            return None

        pkt = {'mode' : 'send'}
        with open(filePath) as fp:
            getId = False
            for line in fp:
                if line.find('class ') > 0:
                    name = self.substring(line, 'class ', ':')
                    pkt['name'] = name.strip()
                    continue

                if not getId:
                    temp = line.replace(' ', '')
                    if temp.find('overrideintgetId') > 0: getId = True
                    continue

                if line.find('return') > 0:
                    uid = self.substring(line, 'return', ';')
                    pkt['id'] = int(uid.strip(), 16)
                    break
        return pkt

    

    def writeKnownPacketFile(self, outpath, packets):
        newlist = sorted(packets, key=lambda k: k['id']) 

        with io.open(outpath, 'w', encoding='utf-8') as f:
            data = json.dumps(newlist, ensure_ascii=False, indent=2)
            f.write(unicode(data))


    
    