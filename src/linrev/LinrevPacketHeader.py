﻿#-*- coding: utf-8 -*-
import os
import io
import json
from Utils import Utils


class LinrevPacketHeader(object):
    
    @staticmethod
    def saveFile(packets, outpath):
        newlist = sorted(packets, key=lambda k: k['id']) 
        with io.open(outpath, 'w', encoding='utf-8') as f:
            data = json.dumps(newlist, ensure_ascii=False, indent=2)
            f.write(unicode(data))


    @staticmethod
    def loadFile(filePath):
        if not os.path.isfile(filePath): return None
        packets = []
        with open(filePath) as fp:
            for line in fp:
                pkt = LinrevPacketHeader.parseLine(line)
                if pkt: packets.append(pkt)
        return packets


    @staticmethod
    def parseLine(line=str):
        pkt = {}
        try:            
            pieces = line.strip().split()
            name = pieces[0].split("::")[0]
            pkt['name'] = name
            pkt['id'] = int(pieces[1], 16)       
            pkt['mode'] = 'send'                 
            if name.endswith('Result'): pkt['mode'] = 'recv'
            return pkt
        except:
            return None
