﻿#-*- coding: utf-8 -*-
import os
import io


class Utils(object):
    @staticmethod
    def getAllFileList(directory, ext=None, recusive=False):
        result = []
        if not directory.endswith('\\'): directory += '\\'
        for dirpath, dirnames, filenames in os.walk(directory):
            for fn in filenames:
                if ext and not fn.endswith(ext): continue
                pathName = os.path.join(dirpath, fn)
                if recusive:
                    pathName = pathName.replace(directory, '')
                result.append(pathName)
        return result


