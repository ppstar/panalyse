﻿import unittest
import os
from linrev.LinrevPacketFinder import LinrevPacketFinder


class Test_TestLinrevPacketFinder(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(Test_TestLinrevPacketFinder, self).__init__(*args, **kwargs)
        self.srcdir = os.getcwd() + '\\data\\lobby\\'



    def test_getRequestPacketFileList(self):
        finder = LinrevPacketFinder()
        files = finder.getRequestPacketFileList(self.srcdir)
        
        self.assertNotEqual(files, None)
        f1 = [s for s in files if s.find("lobby\\request\\PktLobbyLogin.cs") > 0]
        self.assertEqual(len(f1), 1)
        f2 = [s for s in files if s.find("lobby\\request\\PktLobbyVersion.cs") > 0]
        self.assertEqual(len(f2), 1)
        f3 = [s for s in files if s.find("lobby\\bean\\PktLobbyServerInfo.cs") > 0]
        self.assertEqual(len(f3), 0)



    def test_parseRequestPacketFile(self):
        finder = LinrevPacketFinder()
        files = finder.getRequestPacketFileList(self.srcdir)
        
        f1 = [s for s in files if s.find("lobby\\request\\PktLobbyLogin.cs") > 0]
        pkt = finder.parseRequestPacketFile(f1[0])
        
        self.assertNotEqual(pkt, None)
        self.assertEqual(pkt['id'], 0x03)
        self.assertEqual(pkt['name'], 'PktLobbyLogin')
        self.assertEqual(pkt['mode'], 'send')



    def test_generateKnownSendPacketFile(self):
        dirpath = 'Y:\\l2\\projects\\lineage_work_0310_2\\lineage'
        outpath = os.getcwd() + '\\data\\known_send_packets_new.json'
        
        finder = LinrevPacketFinder()
        finder.generateKnownSendPacketFile(dirpath, outpath)
        self.assertEqual(os.path.isfile(outpath), True)
        


    def test_generateKnownRecvPacketFile(self):
        srcpath = os.getcwd() + '\\data\\GameMessagePool.cs'
        outpath = os.getcwd() + '\\data\\known_recv_packets_new.json'

        finder = LinrevPacketFinder()
        finder.generateKnownRecvPacketFile(srcpath, outpath)
        self.assertEqual(os.path.isfile(outpath), True)
        



if __name__ == '__main__':
    unittest.main()
