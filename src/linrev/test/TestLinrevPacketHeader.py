﻿import unittest
from linrev.LinrevPacketHeader import LinrevPacketHeader


class Test_TestLinrevPacketHeader(unittest.TestCase):
    def test_parseLine(self):
        pkt = LinrevPacketHeader.parseLine('PktUIOpenCloseResult::Id		0116')
        self.assertNotEqual(pkt, None)
        self.assertEqual(pkt['name'], 'PktUIOpenCloseResult')
        self.assertEqual(pkt['id'], 0x116)
        self.assertEqual(pkt['mode'], 'recv')

        pkt = LinrevPacketHeader.parseLine('PktSummonGemUse::Id 039D')
        self.assertNotEqual(pkt, None)
        self.assertEqual(pkt['name'], 'PktSummonGemUse')
        self.assertEqual(pkt['id'], 0x39D)
        self.assertEqual(pkt['mode'], 'send')

        pkt = LinrevPacketHeader.parseLine('		5F58')
        self.assertEqual(pkt, None)

        pkt = LinrevPacketHeader.parseLine("Caching 'Functions window'... ok")
        self.assertEqual(pkt, None)


if __name__ == '__main__':
    unittest.main()
