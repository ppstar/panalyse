#-*- coding: utf-8 -*-
import os
from panalyse.detect.DetectPlayer import DetectPlayer
from panalyse.detect.Config import Config


if __name__ == '__main__':
    logpath = 'D:\\ftp_root\\'
    datapath = os.getcwd() + '\\data\\'
    
    config = Config()
    config.packet_header_filepath = datapath + 'packet_header.json'
    config.used_packet_send_filepath = datapath + 'known_send_packets.json'
    config.used_packet_recv_filepath = datapath + 'known_recv_packets.json'

    print("[Info] Start...")

    detectPlayer = DetectPlayer(config)
    detectPlayer.run(logpath)