﻿import os.path
import time
import zlib
from LogFileHandler import LogFileHandler
from KnownPacketHandler import *
from LogFileList import LogFileList
from Config import Config
from panalyse.packet.PacketReader import PacketReader
from panalyse.packet.PacketRepository import PacketRepository
from panalyse.packet.KnownPackets import KnownPackets



class DetectPlayer(object):
    unknwon_packet_filename = 'result_unknown_packets.json'
        

    def __init__(self, config):
        self.config = config
        self.logdir = ''
        self.datadir = ''


    def prepare(self):
        self.logFileHandler = LogFileHandler()
        self.logFileHandler.regist(self.logdir)

        self.knownSendPacket = KnownPacketHandler()
        self.knownSendPacket.regist(self.config.used_packet_send_filepath)
        
        #self.knownRecvPacket = KnownPacketHandler()
        #self.knownRecvPacket.regist(self.config.used_packet_recv_filepath)

        self.logFileList = LogFileList()
        self.logFileList.load(self.logdir + 'detect_logs.txt')
        
        self.unknownPackets = KnownPackets()
        self.loadUnknownPackets()

        self.packetHeaders = KnownPackets()
        self.packetHeaders.loadFile(self.config.packet_header_filepath)

        self.updateUnknownPackets()


    def run(self, logdir):
        self.logdir = logdir
        self.prepare()        

        try:
            while True:
                time.sleep(1)

                if self.knownSendPacket.getTimestamp() != 0:
                    self.updateUnknownPackets()

                filePath = self.logFileHandler.fetchLogFile()
                if not filePath: continue
                
                fileName = os.path.basename(filePath)
                if not self.logFileList.find(fileName):
                    self.processLogFile(filePath)
                    self.logFileList.add(fileName)
        except Exception as ex:
            print("[Error] DectectPlayer: " + str(type(ex)) + ", " + ex.message)


    def updateUnknownPackets(self):
        knownPacketIds = self.knownSendPacket.getPacketIds()
        self.unknownPackets.removePackets(knownPacketIds)
        self.writeUnknownPackets()


    def loadUnknownPackets(self):
        print("[Info] Load unknown packet file")
        try:
            self.unknownPackets.loadFile(self.logdir + self.unknwon_packet_filename)
        except:
            pass

    def writeUnknownPackets(self):
        print("[Info] Write unknown packet file")
        try:
            self.unknownPackets.saveFile(self.logdir + self.unknwon_packet_filename)
        except:
            pass


    def waitFileReadableState(self, filePath):
        for i in range(1, 120):
            try:
                with open(filePath) as fp:
                    return True
            except IOError as ex:
                time.sleep(1)
        return False


    def processLogFile(self, filePath, compressed=True):
        print("[Info] ProcessLogFile: " + os.path.basename(filePath))
        try:
            if not self.waitFileReadableState(filePath):
                raise Exception('File not readable')

            packets = []
            if compressed:
                text = self.decompress(filePath)
                packets = PacketReader.parseText(text, True)
            else:
                packets = PacketReader.readFile(filePath, True)

            if len(packets) <= 0: return
            repo = PacketRepository(packets)
            sendPackets = repo.getSendPackets();

            updated = False
            for pkt in sendPackets:
                if self.knownSendPacket.findPacket(pkt.id): continue
                if self.unknownPackets.incUsedCount(pkt.id): continue
                pkt.count = 1;
                sample = self.packetHeaders.find(pkt.id)
                if sample: pkt.name = sample.name
                self.unknownPackets.append(pkt)
                updated = True

            if updated:
                self.writeUnknownPackets()

        except Exception as ex:
            print("[Error] ProcessLogFile: " + str(type(ex)) + ", " + ex.message)



    def decompress(self, filePath):
        with open(filePath, 'rb') as fp:
            data = fp.read()
            decompresed = zlib.decompress(data)
            return decompresed