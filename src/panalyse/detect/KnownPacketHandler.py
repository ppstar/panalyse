#-*- coding: utf-8 -*-
import os
import threading
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from panalyse.packet.KnownPackets import KnownPackets
from Config import Config


class KnownPacketHandler(FileSystemEventHandler):
    guard = threading.Lock()


    def __init__(self, *args, **kwargs):
        self.timestamp = 0
        self.filepath = ''
        return super(KnownPacketHandler, self).__init__(*args, **kwargs)
    

    def regist(self, filepath):
        self.filepath = filepath

        self.knownPackets = KnownPackets()
        self.loadKnownPackets(filepath)
        
        self.observer = Observer()
        self.observer.schedule(self, os.path.dirname(filepath), recursive=True)
        self.observer.start()
        

    def on_modified(self, event):
        print('[Info] File modifed: ' + os.path.basename(event.src_path))
        if event.src_path == self.filepath:
            self.guard.acquire()
            self.loadKnownPackets(event.src_path)
            self.timestamp += 1
            self.guard.release()
        return super(KnownPacketHandler, self).on_modified(event)


    def loadKnownPackets(self, filepath):
        try:
            self.knownPackets.loadFile(filepath)
        except Exception as ex:
            print('[Error] loadKnownPackets: ' + str(type(ex)) + ", " + ex.message)


    def getTimestamp(self):
        self.guard.acquire()
        result = self.timestamp
        self.timestamp = 0;
        self.guard.release()
        return result


    def findPacket(self, packetId):
        self.guard.acquire()
        pkt = self.knownPackets.find(packetId)
        self.guard.release()
        return pkt


    def getPacketIds(self):
        self.guard.acquire()
        packetIds = self.knownPackets.packetIds()
        self.guard.release()
        return packetIds
