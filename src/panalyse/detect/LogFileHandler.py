#-*- coding: utf-8 -*-
import os
import threading
from collections import deque
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from panalyse.packet.KnownPackets import KnownPackets


class LogFileHandler(FileSystemEventHandler):

    logfiles = []
    guard = threading.Lock()
    

    def __init__(self, *args, **kwargs):
        self.observer = Observer()
        self.dirpath = ''
        return super(LogFileHandler, self).__init__(*args, **kwargs)



    def regist(self, dirpath):
        if not dirpath.endswith('\\'): dirpath += '\\'
        if not os.path.isdir(dirpath): raise Exception('Invalid Directory Path')
        self.logfiles = deque([(dirpath + f) for f in os.listdir(dirpath) if os.path.isfile(dirpath + f) and f.endswith('log.z')]) 

        self.dirpath = dirpath;
        self.observer.schedule(self, dirpath, recursive=True)
        self.observer.start()



    def on_created(self, event):
        print('[Info] File created: ' + os.path.basename(event.src_path))

        filename = os.path.basename(event.src_path)
        if filename.endswith('log.z'):
            self.pushLogFile(event.src_path)

        return super(LogFileHandler, self).on_modified(event)


    def pushLogFile(self, filePath):
        self.guard.acquire()
        self.logfiles.append(filePath)
        self.guard.release()


    def fetchLogFile(self):
        self.guard.acquire()
        filePath = None
        if len(self.logfiles) > 0: filePath = self.logfiles.popleft()
        self.guard.release()
        return filePath
