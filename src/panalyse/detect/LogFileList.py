﻿#-*- coding: utf-8 -*-
import os

class LogFileList(object):
    def __init__(self):
        self.files = []
        self.filePath = ''


    def load(self, filePath):
        self.filePath = filePath
        if not os.path.isfile(filePath): return
        with open(filePath) as f:
            for filename in f:
                filename = filename.strip()
                if len(filename) > 0:
                    self.files.append(filename)

    
    def add(self, filename):
        if self.find(filename): return
        self.files.append(filename)
        
        with open(self.filePath, 'a') as f:
            f.write(filename + '\n')


    def find(self, filename):
        return filename in self.files
