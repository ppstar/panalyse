﻿import unittest
import os
from panalyse.detect.LogFileList import LogFileList


class Test_TestLogFileList(unittest.TestCase):
    def test_load(self):
        logFileList = LogFileList()
        logFileList.load(os.getcwd() + '\\data\\log.txt')
        self.assertEqual(logFileList.find('aaanox-2017-03-12-00.log.z'), False)
        self.assertEqual(logFileList.find('aaanox-2017-03-12-21.log.z'), True)
        self.assertEqual(logFileList.find('aaanox-2017-03-12-22.log.z'), True)

        if not logFileList.find('aaanox-2017-03-12-23.log.z'):
            logFileList.add('aaanox-2017-03-12-23.log.z')
            self.assertEqual(logFileList.find('aaanox-2017-03-12-23.log.z'), True)

            logFileList = LogFileList()
            logFileList.load(os.getcwd() + '\\data\\log.txt')
            self.assertEqual(logFileList.find('aaanox-2017-03-12-23.log.z'), True)


if __name__ == '__main__':
    unittest.main()
