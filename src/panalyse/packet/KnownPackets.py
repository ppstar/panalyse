﻿#-*- coding: utf-8 -*-
import os
import io
import json
from Packet import Packet
from PacketRepository import PacketRepository


class KnownPackets(PacketRepository):
    
    def incUsedCount(self, id):
        pkt = self.find(id)
        if not pkt: return False
        pkt.count += 1
        return True


    def loadFile(self, filePath):
        if not os.path.isfile(filePath): raise Exception('File not exist')
        
        packets = []
        with io.open(filePath) as file:
            packets = json.load(file)

        for kp in packets:
            pkt = Packet()
            pkt.id = kp['id']
            pkt.name = kp['name']
            pkt.mode = kp['mode']
            self.array.append(pkt)


    def saveFile(self, filePath):
        newlist = sorted(self.array, key=lambda pkt: pkt.id) 

        result = []
        for pkt in newlist:
            dt = {}
            dt['id'] = pkt.id
            dt['name'] = pkt.name
            dt['mode'] = pkt.mode
            dt['count'] = pkt.count
            result.append(dt)

        with io.open(filePath, 'w', encoding='utf-8') as f:
            data = json.dumps(result, ensure_ascii=False, indent=2)
            f.write(unicode(data))