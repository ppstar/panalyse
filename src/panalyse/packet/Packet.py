#-*- coding: utf-8 -*-

class Packet(object):
    def __init__(self):
        self.id = 0
        self.name = ''
        self.mode = None
        self.time = ''
        self.count = 0
        self.length = 0
        self.body = []
