﻿#-*- coding: utf-8 -*-
import os.path
from Packet import Packet

class PacketReader(object):
    def __init__(self, *args, **kwargs):
        pass

    @staticmethod
    def readFile(filePath, fastmode=False):
        if not os.path.isfile(filePath):
            print('File not exist')
            return None
         
        with open(filePath) as fp:
            lines = []
            for line in fp:
                if len(line) <= 0: continue
                lines.append(line)

            return PacketReader.parseTextLines(lines, fastmode)           


    @staticmethod
    def parseText(text, fastmode=False):
        lines = text.split('\n')
        return PacketReader.parseTextLines(lines, fastmode)


    @staticmethod
    def parseTextLines(lines, fastmode=False):
        packets = []
        if fastmode:
            for line in lines:
                data = line.lower()
                if data.find('mode') <= 0: continue
                pkt = PacketReader.parseLine(data)
                if pkt is None: raise Exception('Parsing Error')
                packets.append(pkt)
            return packets


        block_list = []
        index = 0
        while index < len(lines):
            data = lines[index].lower()
            if data.find('mode') <= 0: continue
                
            block = [data]
            index += 1
            while index < len(lines):
                data = lines[index].lower()
                if data.find('mode') > 0: break
                #print(data)
                block.append(data)
                index += 1

            block_list.append(block)

        if len(block_list) <= 0:
            return None

        packets = []
        for block in block_list:
            pkt = None
            for data in block:
                sub = PacketReader.parseLine(data)
                if sub is None: raise Exception('Parsing Error')
                if not pkt: pkt = sub
                else: pkt.body.extend(sub.body)

            if pkt != None:
                packets.append(pkt)

        return packets;


   
    # Parse a line
    @staticmethod
    def parseLine(line):
        if not line or len(line) <= 0: return None
        data = line.lower()
        
        pkt = Packet()        
        try:       
            index = data.find('mode')
            if index > 0:
                mdex = data.find(' : ')
                if mdex <= 0:
                    raise Exception('Invalid Time Format')
                pkt.time = data[:mdex]

                data = data[index:]
                pair = dict(s.strip().split('=', 1) for s in data.split(','))
                pkt.mode = pair['mode'].replace('[', '').replace(']', '')
                pkt.id = int(pair['header'], 16)
                pkt.length = int(pair['length'], 16)
                return pkt

            else:
                pkt.body = [int(s.strip(), 16) for s in data.split()]
                return pkt

        except Exception:
            raise Exception('Invalid Format')
