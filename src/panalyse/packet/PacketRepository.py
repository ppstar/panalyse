﻿#-*- coding: utf-8 -*-

class PacketRepository(object):
    def __init__(self, *args, **kwargs):
        self.array = []
        if len(args) > 0:
            self.array = args[0]


    def size(self):
        return len(self.array)


    def get(self, index):
        return self.array[index]


    def getArray(self):
        return self.array


    def append(self, pkt):
        self.array.append(pkt)


    def remove(self, pkt):
        self.array.remove(pkt)


    def removePackets(self, idList=list):
        self.array = [pkt for pkt in self.array if not pkt.id in idList]


    def find(self, id):
        return next((x for x in self.array if x.id == id), None)

    
    def findPackets(self, idList=list):
        packets = [pkt for pkt in self.array if pkt.id in idList]
        return packets


    def packetIds(self):
        values = [pkt.id for pkt in self.array]
        return sorted(list(set(values)))


    def getPackets(self, id):
        packets = [pkt for pkt in self.array if pkt.id == id]
        return packets


    def getSendPackets(self):
        packets = [pkt for pkt in self.array if pkt.mode == 'send']
        return packets


    def getRecvPackets(self):
        packets = [pkt for pkt in self.array if pkt.mode == 'recv']
        return packets