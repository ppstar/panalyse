﻿import unittest
import os
from panalyse.packet.KnownPackets import KnownPackets


class Test_TestKnownPackets(unittest.TestCase):
    
    def test_knownpackets_loadFile(self):
        packets = KnownPackets()
        packets.loadFile(os.getcwd() + '\\data\\known_send_packets.json')
        
        pkt = packets.find(25)
        self.assertNotEqual(pkt, None)
        self.assertEqual(pkt.id, 25)
        self.assertEqual(pkt.name, 'PktPlayerSelect')


    def test_knownpackets_saveFile(self):
        packets = KnownPackets()
        packets.loadFile(os.getcwd() + '\\data\\known_send_packets.json')

        packets.saveFile(os.getcwd() + '\\data\\test_save.json')


if __name__ == '__main__':
    unittest.main()
