﻿import unittest
import os
from panalyse.packet.PacketReader import PacketReader
from panalyse.packet.PacketRepository import PacketRepository


class Test_TestPacketReader(unittest.TestCase):

    def test_pacetreader_parseLine(self):
        pkt = PacketReader.parseLine('')
        self.assertEqual(pkt, None)

        pkt = PacketReader.parseLine("03-11 22:20:00 : Mode=Send, Header=29, Length=7C")
        self.assertNotEqual(pkt, None)
        self.assertEqual(pkt.time, "03-11 22:20:00")
        self.assertEqual(pkt.mode, 'send')
        self.assertEqual(pkt.id, 0x29)
        self.assertEqual(pkt.length, 0x7C)

        pkt = PacketReader.parseLine("03-11 22:20:00 : Mode=Recv, Header=29, Length=7C")
        self.assertEqual(pkt.mode, 'recv')

        with self.assertRaises(Exception) as context:
            pkt = PacketReader.parseLine("03-11 22:20:00 : Mode=Send Header=29 Length=7C")
        self.assertRaises('Invalid Format' in context.exception)
        
        with self.assertRaises(Exception) as context:
            pkt = PacketReader.parseLine("7C,00 00 42 C6 87 C4 29 00 00 00 06 00 30 2E 34")
        self.assertRaises('Invalid Format' in context.exception)

        pkt = PacketReader.parseLine("7C 00 00 42 C6 87 C4 29 00 00 00 06 00 30 2E 34")
        self.assertNotEqual(pkt, None)
        self.assertEqual(len(pkt.body), 0x10)
        self.assertEqual(pkt.body[0], 0x7C)
        self.assertEqual(pkt.body[3], 0x42)
        self.assertEqual(pkt.body[15], 0x34)


        pkt = PacketReader.parseLine("7C 00 00 42 C6")
        self.assertNotEqual(pkt, None)
        self.assertEqual(len(pkt.body), 0x05)
        self.assertEqual(pkt.body[4], 0xC6)



    def test_pacetreader_readFile(self):
        sample_file = os.getcwd() + '\\data\\sample_1.txt'

        array = PacketReader.readFile(sample_file)
        self.assertNotEqual(array, None)

        packets = PacketRepository(array)
        self.assertEqual(packets.size(), 2)
        self.assertEqual(packets.get(0).length, 0x7C)
        self.assertEqual(packets.get(0).length, len(packets.get(0).body))
        self.assertEqual(packets.get(0).body[0x10], 0x30)
        self.assertEqual(packets.get(0).body[0x20], 0x5F)
        self.assertEqual(packets.get(1).length, len(packets.get(1).body))
        self.assertEqual(packets.get(1).body[0x10], 0x17)
                
        array = PacketReader.readFile(sample_file, True)
        self.assertNotEqual(array, None)

        packets = PacketRepository(array)
        self.assertEqual(packets.size(), 2)
        self.assertEqual(packets.get(0).length, 0x7C)
        self.assertEqual(len(packets.get(0).body), 0)


if __name__ == '__main__':
    unittest.main()
