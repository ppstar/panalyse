﻿import unittest
from panalyse.packet.Packet import Packet
from panalyse.packet.PacketRepository import PacketRepository


class Test_TestPacketRepository(unittest.TestCase):
    def test_packetrepository(self):
        rep = PacketRepository()
        
        pkt = Packet()
        pkt.id = 10
        rep.append(pkt)

        pkt = Packet()
        pkt.id = 6
        rep.append(pkt)

        pkt = Packet()
        pkt.id = 16
        pkt.name = 'pkt3'
        rep.append(pkt)

        pkt = Packet()
        pkt.id = 16
        pkt.name = 'pkt4'
        rep.append(pkt)

        self.assertEqual(rep.size(), 4)

        result = rep.packetIds()
        self.assertEqual(len(result), 3)
        self.assertEqual(result[0], 6)
        self.assertEqual(result[1], 10)
        self.assertEqual(result[2], 16)


        result = rep.getPackets(16)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0].name, 'pkt3')
        self.assertEqual(result[1].name, 'pkt4')




if __name__ == '__main__':
    unittest.main()
